# playbook to install nginx for galaxy
---
- hosts: galaxy_nginx
  tags: nginx
  vars:
    hostname: "{{ ansible_hostname }}"
    nginx_remove_default_vhost: true
    nginx_client_max_body_size: "20g"
    nginx_server_names_hash_bucket_size: "128"
    nginx_user: galaxy
    nginx_vhosts:
      - listen: "80"
        server_name: "{{ hostname }}"
        extra_parameters: |
          return 301 https://$host$request_uri;
        filename: "{{ hostname }}.80.conf"
      - listen: "443 ssl default_server"
        server_name: "{{ hostname }}"
        access_log: "/var/log/nginx/access.log"
        error_log: "/var/log/nginx/error.log"
        state: "present"
        filename: "{{ hostname }}.443.conf"
        extra_parameters: |
          ssl_protocols TLSv1.2;
          ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
          ssl_prefer_server_ciphers on;
          ssl_ecdh_curve secp384r1:prime256v1;
          ssl_certificate "{{ galaxy_frontend_ssl_cert }}" ;
          ssl_certificate_key "{{ galaxy_frontend_ssl_key }}" ;
          client_max_body_size 10G; # aka max upload size, defaults to 1M
          uwsgi_read_timeout 2400;
          uwsgi_max_temp_file_size 0;
          location /training-material/ {
              proxy_pass https://training.galaxyproject.org/training-material/;
          }
          location / {
             uwsgi_pass "{{ groups.galaxy | first }}:4001" ;
             include uwsgi_params;
             uwsgi_force_ranges on;
             uwsgi_param HTTPS on;
             uwsgi_param REQUEST_SCHEME https;

             # Maintenance mode
             #satisfy any;
             # Allow gitlab-ci to access
             #allow 192.54.201.184;
             #deny all;
             # Allow by passwd
             #auth_basic "Access restricted during maintenace";
             #auth_basic_user_file /etc/nginx/.htpasswd;

          }
          location /_x_accel_redirect/ {
            internal;
            alias /;
          }
          #location /static/welcome.html {
          #  alias /srv/welcome/$host/;
          #}
          #location ~ /static/style/base.css {
          #  root /srv/base-css;
          #  try_files /$host.css /{{ galaxy_domain }}.css =404;
          #}
          location /static/welcome.html {
                 alias {{ subdomains_dir }}/$host.html;
          }
          location /static {
                  alias {{ galaxy_server_dir }}/static;
                  expires 24h;
          }
          location ~ /static/style/base.css {
              root {{ subdomains_dir }}/;
              try_files /$host.css /{{ galaxy_domain }}.css =404;
          }
          location /reports {
              uwsgi_pass "{{ groups.galaxy | first }}:9001";
              uwsgi_param UWSGI_SCHEME $scheme;
              include uwsgi_params;
              # satisfy any;
              # allow all;
              auth_basic galaxy;
              auth_basic_user_file /etc/nginx/.reports_passwd;
              uwsgi_param HTTP_REMOTE_USER $remote_user;
              uwsgi_param HTTP_GX_SECRET SOME_SECRET_STRING;
          }


      - listen: "443 ssl"
        server_name: "*.interactivetool.{{ galaxy_domain }}"
        access_log: "/var/log/nginx/galaxy-gie-proxy-access.log"
        error_log: "/var/log/nginx/galaxy-gie-proxy-error.log"
        state: "present"
        filename: "{{ hostname }}.gie-proxy.conf"
        extra_parameters: |
          ssl_protocols TLSv1.2;
          ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
          ssl_prefer_server_ciphers on;
          ssl_ecdh_curve secp384r1:prime256v1;
          ssl_certificate "{{ galaxy_frontend_ssl_cert }}" ;
          ssl_certificate_key "{{ galaxy_frontend_ssl_key }}" ;
          client_max_body_size 10G; # aka max upload size, defaults to 1M
          location / {
             proxy_redirect off;
             proxy_http_version 1.1;
             proxy_set_header Host $host;
             proxy_set_header X-Real-IP $remote_addr;
             proxy_set_header Upgrade $http_upgrade;
             proxy_set_header Connection "upgrade";
             proxy_pass http://{{ groups.galaxy | first }}:{{ gie_proxy_port }};
             proxy_read_timeout 1d;
             proxy_buffering off;
          }

  pre_tasks:
    - name: galaxy nginx | Install prerequisite for openssl_certificate module
      package:
        name: python-openssl
        state: present
      when:
        - galaxy_frontend_create_self_signed_cert is defined and galaxy_frontend_create_self_signed_cert
    - name: galaxy nginx | Generate an openssl private key (size 4096 bits, type RSA)
      openssl_privatekey:
        path: "{{ galaxy_frontend_ssl_key }}"
      when:
        - galaxy_frontend_create_self_signed_cert is defined and galaxy_frontend_create_self_signed_cert
    - name: galaxy nginx | Generate an openssl Certificate Signing Request
      openssl_csr:
        path: "{{ galaxy_frontend_ssl_csr }}"
        privatekey_path: "{{ galaxy_frontend_ssl_key }}"
        country_name: FR
        state_or_province_name: Bas-Rhin
        locality_name: Illkirch
        organization_name: POC
        common_name: "{{ galaxy_domain }}"
      when:
        - galaxy_frontend_create_self_signed_cert is defined and galaxy_frontend_create_self_signed_cert
    - name: galaxy nginx | Create self signed certificate (expires in 10 years)
      openssl_certificate:
        path: "{{ galaxy_frontend_ssl_cert }}"
        privatekey_path: "{{ galaxy_frontend_ssl_key }}"
        csr_path: "{{ galaxy_frontend_ssl_csr }}"
        provider: selfsigned
      when:
        - galaxy_frontend_create_self_signed_cert is defined and galaxy_frontend_create_self_signed_cert
    - name: galaxy nginx |  Copy SSL certificate (prod)
      copy:
        content: "{{ star_usegalaxy_chained_ssl_certificate }}"
        dest: "{{ galaxy_frontend_ssl_cert }}"
        owner: root
        group: root
        mode: "0644"
      when: inventory_file is match(".*/production/hosts")
    - name: galaxy nginx  | Copy private key into correct folder for nginx (prod)
      copy:
        content: "{{ star_usegalaxy_ssl_key_content }}"
        dest: "{{ galaxy_frontend_ssl_key }}"
        remote_src: yes
        owner: root
        group: ssl-cert
        mode: 0440
      when: inventory_file is match(".*/production/hosts")
    # Workaround "Galaxy access from compute node"
    # Error: https://ifbtaskforce.slack.com/archives/CQY4BJQTC/p1603125488005700
    # https://access.redhat.com/solutions/53031
    # Set irp_filter in Loose mode (2) in /proc and in the sysctl file and reload if necessary
    - name: Workaround Galaxy access from compute node (sysctl rp_filter=2)
      sysctl:
        name: net.ipv4.conf.all.rp_filter
        value: "2"
        sysctl_set: yes
        state: present
        reload: yes
    - name: galaxy nginx  |  Install Pip for the htpasswd Ansible module
      package:
        name: python-pip
      become: yes
    - name : galaxy nginx  |  Pip install passlib
      pip:
        name: passlib
    - name: galaxy nginx  |  Create password file for Reports
      htpasswd:
        path: /etc/nginx/.reports_passwd
        name: "{{ reports_admin_user }}"
        password: "{{ reports_admin_password }}"
        owner: www-data
        group: root
        mode: 644
  roles:
    - role: geerlingguy.nginx
      become: True
    - role: welcome_pages
      become: True
