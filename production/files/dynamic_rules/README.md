# Sorting hat

This is the code and conf for using the sorting hat.

The python code comes from https://github.com/usegalaxy-eu/sorting-hat/, copying it here to allow possible custom patches in the future.

The yaml files are the specific configuration for usegalaxy.fr, adapted from the original one, and [the one from usegalaxy.eu](https://github.com/usegalaxy-eu/infrastructure-playbook/blob/master/files/galaxy/dynamic_rules/usegalaxy/)

## Modifying the destination for a tool

In `destination_specifications.yaml`, some general destinations are defined.

In `tool_destinations.yaml` you will find specifications for each tool. Each tool can get assigned a number of cores/mem/env vars, and a general destination defined in `destination_specifications.yaml`.

In `sorting_hat.yaml`, the default destination is defined.

The sorting hat code then takes all this conf into account to create on the fly a specific destination for each job submitted.
