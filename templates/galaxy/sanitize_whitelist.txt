toolshed.g2.bx.psu.edu/repos/crs4/taxonomy_krona_chart/taxonomy_krona_chart/2.7.1

toolshed.g2.bx.psu.edu/repos/devteam/fastqc/fastqc/0.72+galaxy1
toolshed.g2.bx.psu.edu/repos/devteam/fastqc/fastqc/0.73+galaxy0

toolshed.g2.bx.psu.edu/repos/ethevenot/heatmap/Heatmap/2.2.2

toolshed.g2.bx.psu.edu/repos/fgiacomoni/hmdb_ms_search/wsdl_hmdb/1.6.1

toolshed.g2.bx.psu.edu/repos/iuc/aegean_parseval/aegean_parseval/0.16.0
toolshed.g2.bx.psu.edu/repos/iuc/fastp/fastp/0.20.1+galaxy0
toolshed.g2.bx.psu.edu/repos/iuc/fastqe/fastqe/0.2.6+galaxy2
toolshed.g2.bx.psu.edu/repos/iuc/funannotate_compare/funannotate_compare/1.8.9+galaxy1
toolshed.g2.bx.psu.edu/repos/iuc/funannotate_compare/funannotate_compare/1.8.9+galaxy2
toolshed.g2.bx.psu.edu/repos/iuc/ggplot2_heatmap2/ggplot2_heatmap2/3.0.1
toolshed.g2.bx.psu.edu/repos/iuc/jbrowse/jbrowse/1.16.11+galaxy1
toolshed.g2.bx.psu.edu/repos/iuc/mothur_venn/mothur_venn/1.39.5.0
toolshed.g2.bx.psu.edu/repos/iuc/multiqc/multiqc/1.7
toolshed.g2.bx.psu.edu/repos/iuc/multiqc/multiqc/1.8+galaxy1
toolshed.g2.bx.psu.edu/repos/iuc/multiqc/multiqc/1.9+galaxy1
toolshed.g2.bx.psu.edu/repos/iuc/multiqc/multiqc/1.11+galaxy0
toolshed.g2.bx.psu.edu/repos/iuc/nanoplot/nanoplot/1.28.2+galaxy1
toolshed.g2.bx.psu.edu/repos/iuc/pycoqc/pycoqc/2.5.2+galaxy0
toolshed.g2.bx.psu.edu/repos/iuc/quast/quast/5.0.2+galaxy0
toolshed.g2.bx.psu.edu/repos/iuc/quast/quast/5.0.2+galaxy2
toolshed.g2.bx.psu.edu/repos/iuc/quast/quast/5.0.2+galaxy3

toolshed.g2.bx.psu.edu/repos/lecorguille/xcms_summary/abims_xcms_summary/3.4.4.0

toolshed.g2.bx.psu.edu/repos/prog/mtblsdwnld/mtbls-dwnld/4.1.4

toolshed.g2.bx.psu.edu/repos/proteore/proteore_heatmap_visualization/heatmap/2019.06.27
toolshed.g2.bx.psu.edu/repos/proteore/proteore_heatmap_visualization/heatmap/2020.01.23
toolshed.g2.bx.psu.edu/repos/proteore/proteore_maps_visualization/kegg_maps_visualization/2019.06.27.1
toolshed.g2.bx.psu.edu/repos/proteore/proteore_maps_visualization/kegg_maps_visualization/2020.03.05
toolshed.g2.bx.psu.edu/repos/proteore/proteore_go_terms_enrich_comparison/go_terms_enrich_comparison/2020.02.04
toolshed.g2.bx.psu.edu/repos/proteore/proteore_reactome/reactome_analysis/2019.06.27
toolshed.g2.bx.psu.edu/repos/proteore/proteore_reactome/reactome_analysis/2020.01.23
toolshed.g2.bx.psu.edu/repos/proteore/proteore_venn_diagram/Jvenn/2019.09.12
toolshed.g2.bx.psu.edu/repos/proteore/proteore_venn_diagram/Jvenn/2020.01.10

toolshed.g2.bx.psu.edu/repos/saskia-hiltemann/krona_text/krona-text/1
